﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RedPillService.Contract
{
    [System.Runtime.Serialization.DataContractAttribute(Name = "ContactDetails", Namespace = "http://KnockKnock.readify.net")]
    [System.SerializableAttribute()]
    public class ContactDetails : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged
    {
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;

        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string EmailAddressField;

        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string FamilyNameField;

        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string GivenNameField;

        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string PhoneNumberField;

        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData
        {
            get
            {
                return this.extensionDataField;
            }
            set
            {
                this.extensionDataField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string EmailAddress
        {
            get
            {
                return this.EmailAddressField;
            }
            set
            {
                if ((object.ReferenceEquals(this.EmailAddressField, value) != true))
                {
                    this.EmailAddressField = value;
                    this.RaisePropertyChanged("EmailAddress");
                }
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string FamilyName
        {
            get
            {
                return this.FamilyNameField;
            }
            set
            {
                if ((object.ReferenceEquals(this.FamilyNameField, value) != true))
                {
                    this.FamilyNameField = value;
                    this.RaisePropertyChanged("FamilyName");
                }
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string GivenName
        {
            get
            {
                return this.GivenNameField;
            }
            set
            {
                if ((object.ReferenceEquals(this.GivenNameField, value) != true))
                {
                    this.GivenNameField = value;
                    this.RaisePropertyChanged("GivenName");
                }
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string PhoneNumber
        {
            get
            {
                return this.PhoneNumberField;
            }
            set
            {
                if ((object.ReferenceEquals(this.PhoneNumberField, value) != true))
                {
                    this.PhoneNumberField = value;
                    this.RaisePropertyChanged("PhoneNumber");
                }
            }
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
}