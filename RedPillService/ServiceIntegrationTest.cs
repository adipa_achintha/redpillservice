﻿using System;
using System.ServiceModel;
using Contracts.RedPill;
using NUnit.Framework;


namespace RedPillService
{
    [TestFixture]
    public class ServiceIntegrationTest
    {
        private ChannelFactory<IRedPill> _redPill;

        [SetUp]
        public void SetUp()
        {
            //_redPill = new ChannelFactory<IRedPill>(new BasicHttpsBinding(), new EndpointAddress("https://knockknock.readify.net/RedPill.svc"));
            _redPill = new ChannelFactory<IRedPill>(new BasicHttpBinding(), new EndpointAddress("http://localhost:57139/RedPillService.svc"));
            //_redPill = new ChannelFactory<IRedPill>(new BasicHttpBinding(), new EndpointAddress("http://168.63.166.97/redpill/RedPillService.svc"));
        }

        [Test]
        public void TestWhoAreYou()
        {
            var value = _redPill.CreateChannel().WhoAreYou();

            Assert.IsNotNull(value);
            Assert.IsNotNullOrEmpty(value.EmailAddress);
            Assert.IsNotNullOrEmpty(value.FamilyName);
            Assert.IsNotNullOrEmpty(value.GivenName);
            Assert.IsNotNullOrEmpty(value.PhoneNumber);
        }

        [Test]
        public void TestFibonacciNumber()
        {
            var value = _redPill.CreateChannel().FibonacciNumber(0);
            
            Assert.AreEqual(value,0);
            value = _redPill.CreateChannel().FibonacciNumber(1);
            Assert.AreEqual(value, 1);
            value = _redPill.CreateChannel().FibonacciNumber(2);
            Assert.AreEqual(value, 1);
            value = _redPill.CreateChannel().FibonacciNumber(3);
            Assert.AreEqual(value, 2);
            value = _redPill.CreateChannel().FibonacciNumber(4);
            Assert.AreEqual(value, 3);

            try
            {
                value = _redPill.CreateChannel().FibonacciNumber(1000000000);
                Assert.Fail("Should throw an error");
            }
            catch (Exception ex)
            {
            }
        }

        [Test]
        public void TestWhatShapeIsThis()
        {
            var value = _redPill.CreateChannel().WhatShapeIsThis(1, 1, 1);
            Assert.AreEqual(value, TriangleType.Equilateral);

            value = _redPill.CreateChannel().WhatShapeIsThis(2, 2, 1);
            Assert.AreEqual(value, TriangleType.Isosceles);

            value = _redPill.CreateChannel().WhatShapeIsThis(2, 3, 4);
            Assert.AreEqual(value, TriangleType.Scalene);

            value = _redPill.CreateChannel().WhatShapeIsThis(1, 2, 3);
            Assert.AreEqual(value, TriangleType.Error);
        }

        [Test]
        public void TesReverseWords()
        {
            var value = _redPill.CreateChannel().ReverseWords("aab bbc cca");
            Assert.AreEqual("baa cbb acc", value);
        }
    }
}