﻿using System;
using System.IO;

namespace RedPillService
{
    public class FileLogger
    {
        private static Object _lock = new object();

        public static void Log(string log)
        {
            lock (_lock)
            {
                StreamWriter writer;
                var path = "C:\\temp\\logs.txt";
                if (!File.Exists(path))
                {
                    writer = File.CreateText(path);
                }
                else
                {
                    writer = new StreamWriter(path, true);
                }
                writer.WriteLine(log);
                writer.Flush();
                writer.Close();
            }
        }
    }
}