﻿using System;
using System.Text;
using Contracts.RedPill;


namespace RedPillService
{
    public class RedPillService : IRedPill
    {
        public ContactDetails WhoAreYou()
        {
            FileLogger.Log("Who are you...?");
            return new ContactDetails()
                       {
                           EmailAddress = "adipa.achintha@gmail.com",
                           FamilyName = "Gunasekara",
                           GivenName = "Adipa",
                           PhoneNumber = "0449568523"
                       };
        }

        public long FibonacciNumber(long n)
        {
            FileLogger.Log(string.Format("FibonacciNumber...{0}",n));

            int sign = (n < 0 && n % 2 == 0) ? -1 : 1;
            n = Math.Abs(n);

            int a = 0;
            int b = 1;

            for (long i = 0; i < n; i++)
            {
                int temp = a;
                a = b;
                if (long.MaxValue - b < a)
                {
                    throw new ArgumentOutOfRangeException();
                }
                b = temp + b;
            }
            return sign * a;
        }

        public TriangleType WhatShapeIsThis(int a, int b, int c)
        {
            FileLogger.Log(string.Format("WhatShapeIsThis...{0}_{1}_{2}", a,b,c));
            if (a < 0 || b < 0 || c< 0 
                || a + b <= c || a + c <= b || b + c <= a)
                return TriangleType.Error;
            if (a == b && b == c)
                return TriangleType.Equilateral;
            if (a == b || a == c || b == c)
                return TriangleType.Isosceles;
            return TriangleType.Scalene;
        }

        public string ReverseWords(string s)
        {
            FileLogger.Log(string.Format("ReverseWords...{0}", s));
            if (s == null)
                throw new ArgumentNullException();

            var sb = new StringBuilder();
            string[] values = s.Split(' ');
            foreach (var value in values)
            {
                var array = value.ToCharArray();
                Array.Reverse(array);
                if (sb.Length > 0)
                    sb.Append(' ');
                sb.Append(new string(array));
            }
            return sb.ToString();
        }
    }
}
