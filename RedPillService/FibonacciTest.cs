﻿using System;
using NUnit.Framework;

namespace RedPillService
{
    [TestFixture]
    public class FibonacciTest
    {
        [TestCase(0,0)]
        [TestCase(1,1)]
        [TestCase(2,1)]
        [TestCase(3,2)]
        [TestCase(8,21)]
        [TestCase(16,987)]
        [TestCase(-1, 1)]
        [TestCase(-2, -1)]
        [TestCase(-3, 2)]
        [TestCase(-4, -3)]
        [TestCase(-8, -21)]
        [TestCase(-16, -987)]
        public void TestNormalOperation(long sequence,long expected)
        {
            var service = new RedPillService();
            long val = service.FibonacciNumber(sequence);
            Assert.AreEqual(expected, val);
        }

        [TestCase(long.MaxValue)]
        [TestCase(long.MinValue)]
        public void TestForBoundaryConditions(long sequence)
        {
            var service = new RedPillService();
            try
            {
                service.FibonacciNumber(sequence);
                Assert.Fail("Should throw an exception");
            }
            catch (Exception ex)
            {
            }   
        }
    }
}