﻿using System.Runtime.Serialization;

namespace Contracts.RedPill
{
    [DataContract(Namespace = ServiceConstants.Namespace)]
    public enum TriangleType
    {
        [EnumMember]
        Error = 0,
        
        [EnumMember]
        Equilateral = 1,
        
        [EnumMember]
        Isosceles = 2,
        
        [EnumMember]
        Scalene = 3,
    }
}
