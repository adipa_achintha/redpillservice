﻿using System;
using System.ServiceModel;

namespace Contracts.RedPill
{
    [ServiceContract(Namespace = ServiceConstants.Namespace,Name = ServiceConstants.Name )]
    public interface IRedPill
    {
        [OperationContract]
        ContactDetails WhoAreYou();

        [OperationContract]
        [FaultContract(typeof(ArgumentOutOfRangeException))]
        long FibonacciNumber(long n);

        [OperationContract]
        TriangleType WhatShapeIsThis(int a, int b, int c);

        [OperationContract]
        [FaultContract(typeof(ArgumentNullException))]
        string ReverseWords(string s);
    }
}
