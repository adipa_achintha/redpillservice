﻿using System.Runtime.Serialization;

namespace Contracts.RedPill
{
    [DataContract(Namespace = ServiceConstants.Namespace)]
    public class ContactDetails
    {
        [DataMember]
        public string EmailAddress { get; set; }

        [DataMember]
        public string FamilyName { get; set; }

        [DataMember]
        public string GivenName { get; set; }

        [DataMember]
        public string PhoneNumber { get; set; }
    }
}
